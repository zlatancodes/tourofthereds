//
//  ViewController.m
//  tourofthereds
//
//  Created on 10/07/17.
//

#import "ViewController.h"
#import <Beaconstac/Beaconstac.h>
#import "WebViewController.h"
#import "MusicCardView.h"
#import "SummaryCardView.h"
#import "PhotoCardView.h"
#import "PageCardView.h"

@interface ViewController ()<BeaconstacDelegate, MusicCardDelegate, SummaryCardDelegate, YTPlayerViewDelegate,PageCardDelegate, PhotoCardDelegate, MSWebhookDelegate>
{
    NSString *mediaType;
    NSURL *mediaUrl;
    Beaconstac *beaconstac;
    MSCard *visibleCard;
    MusicCardView *musicCardView;
    SummaryCardView *summaryCardView;
    PhotoCardView *photoCardView;
    PageCardView *pageCardView;
}

@property (weak, nonatomic) UIView *visibleCardView;
@property (nonatomic, strong) NSMutableDictionary *notificationsDictionary;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Tour of The Reds";
    
    beaconstac = [Beaconstac sharedInstanceWithOrganizationId:720 developerToken:@"e62435a78e67ec98bba3b879ba00448650032557"];
    beaconstac.usageRequirement = (UsageAllowRangingInBackground | UsageRegionMonitoringRequired | UsageRangeOnDisplayWakeUp);
    [beaconstac setDelegate:self];
    [beaconstac setUserIdentityWithFirstName:nil lastName:nil emailAddress:nil userInfo:nil];
    
    // Start ranging beacons with the given UUID
    [beaconstac startRangingBeaconsWithUUIDString:@"8492E75F-4FD6-469D-B132-043FE94921D8" beaconIdentifier:@"ManchesterUnited" filterOptions:@{@"mybeacons":@YES}];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    
    //SELF: Fetch All Notifications from Backend and Register
    
}

#pragma mark - Beacons delegate methods


- (void)beaconstac:(Beaconstac *)beaconstac rangedBeacons:(NSDictionary *)beaconsDictionary
{
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        return;
    }
}

- (void)beaconstac:(Beaconstac *)beaconstac didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
}

- (void)beaconstac:(Beaconstac*)beaconstac campedOnBeacon:(MSBeacon*)beacon amongstAvailableBeacons:(NSDictionary *)beaconsDictionary
{
    NSLog(@"CampedOnBeacon: %@", beacon);
}

- (void)beaconstac:(Beaconstac*)beaconstac exitedBeacon:(MSBeacon*)beacon
{
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        return;
    }
}

- (void)beaconstac:(Beaconstac*)beaconstac didEnterBeaconRegion:(CLRegion*)region
{
    int type = 2; //Currently always TRUE till Backend integration
    
    //Also, a test Summary Card is being added by us for the Hackathon for checking if it's working fine (later we shall integrate it with the backed.)
    
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        NSLog(@"Entered beacon region :%@", region.identifier);
        if (type == 1){
            summaryCardView = [[[NSBundle mainBundle] loadNibNamed:@"SummaryCardView"
                                                             owner:self
                                                           options:nil] firstObject];
            MSCard *card;
            MSNotification *notif = [self.notificationsDictionary objectForKey:card.notification];
            [self.view addSubview:summaryCardView];
            summaryCardView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            summaryCardView.layer.masksToBounds = YES;
            summaryCardView.delegate = self;
            summaryCardView.titleLabel.text = visibleCard.title;
            summaryCardView.summaryView.text = visibleCard.body;
            [summaryCardView.okButton setTitle:notif.okLabel?:@"OK" forState:UIControlStateNormal];
            summaryCardView.summaryView.text = visibleCard.body;
            summaryCardView.okAction = notif.okAction;
            [UIView animateWithDuration:0.35 animations:^{
                summaryCardView.frame = self.view.frame;
                self.navigationController.navigationBarHidden = YES;
            } completion:^(BOOL finished) {
            }];
            NSURL *url = [NSURL URLWithString:@"BACKEND_URL"];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                       if (!error ) {
                                           UIImage *fetchedImage = [UIImage imageWithData:data];
                                           if (fetchedImage) {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   summaryCardView.imageView.image = fetchedImage;
                                               });
                                           }
                                       }
                                   }];
            
            self.visibleCardView = summaryCardView;
            [UIView animateWithDuration:0.35 animations:^{
                summaryCardView.frame = self.view.frame;
                self.navigationController.navigationBarHidden = YES;
            } completion:^(BOOL finished) {
            }];

        }
        else if (type == 2){
            MSCard *card;
            MSNotification *notif = [self.notificationsDictionary objectForKey:card.notification];
            
            if (!musicCardView) {
                musicCardView = [[[NSBundle mainBundle] loadNibNamed:@"MusicCardView"
                                                               owner:self
                                                             options:nil] firstObject];
            }
            [self.view addSubview:musicCardView];
            musicCardView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            musicCardView.layer.masksToBounds = YES;
            musicCardView.delegate = self;
            musicCardView.titleLabel.text = @"Welcome to Manchester United";
            musicCardView.media = [card.mediaArray firstObject];
            [musicCardView.okButton setTitle:notif.okLabel?:@"OK" forState:UIControlStateNormal];
            musicCardView.okAction = notif.okAction;
            
            self.visibleCardView = musicCardView;
            [UIView animateWithDuration:0.35 animations:^{
                musicCardView.frame = self.view.frame;
                self.navigationController.navigationBarHidden = YES;
            } completion:^(BOOL finished) {
                musicCardView.ytPlayerView.delegate = self;
                NSURL *url = [NSURL URLWithString:@"https://www.youtube.com/watch?v=XWA3fkhTCfs"];
                [musicCardView loadYoutubeWithUrl:url];
            }];
        }
        return;
    }
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = [NSString stringWithFormat:@"Entered beacon region: %@", region.identifier];
    [[UIApplication sharedApplication]presentLocalNotificationNow:notification];
}

- (void)beaconstac:(Beaconstac*)beaconstac didExitBeaconRegion:(CLRegion *)region
{
    if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) {
        NSLog(@"Exited beacon region :%@", region.identifier);
        return;
    }
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = [NSString stringWithFormat:@"Exited beacon region: %@", region.identifier];
    [[UIApplication sharedApplication]presentLocalNotificationNow:notification];
}

//SELF
// Tells the delegate that a rule is triggered with corresponding list of actions.
- (void)beaconstac:(Beaconstac *)beaconstac triggeredRuleWithRuleName:(NSString *)ruleName actionArray:(NSArray *)actionArray
{
    NSLog(@"triggeredRuleWithRuleName: %@", ruleName);
    // actionArray contains the list of actions to trigger for the rule that matched.
    
    for (MSAction *action in actionArray) {
        // action type can be "popup", "webpage", "card", "media", or even "custom"
        // We will focus mainly on Card and Media now.
        
        switch (action.type) {
                
            case MSActionTypeWebpage:
            {
                [self performSegueWithIdentifier:@"webSegue" sender:(MSWebpageAction*)action];
                NSLog(@"Webpage action type");
            }
                break;
                
            case MSActionTypeCard:
            {
                visibleCard = (MSCard*)action;
                if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground) {
                    MSNotification *notif = [self.notificationsDictionary objectForKey:visibleCard.notification];
                    if (notif) {
                        [notif showInApplication:[UIApplication sharedApplication]];
                    }
                } else if (self.visibleCardView) {
                    NSLog(@"Ignoring card");
                } else {
                    [self showCard:visibleCard];
                }
            }
                break;
                
            case MSActionTypeNotification:
            {
                MSNotification *notify = (MSNotification*)action;
                [notify showInApplication:[UIApplication sharedApplication]];
            }
                
            default:
                break;
        }
    }
}

#pragma mark - Music Card delegate
- (void)mediaCardButtonClickedAtIndex:(int)index
{
    switch (index) {
        case 0:
        {
            [UIView animateWithDuration:0.35 animations:^{
                self.visibleCardView.frame = CGRectMake(0, musicCardView.frame.size.height,musicCardView.frame.size.width, musicCardView.frame.size.height);
            } completion:^(BOOL finished) {
                [musicCardView removeFromSuperview];
                self.visibleCardView = nil;
                self.navigationController.navigationBarHidden = NO;
            }];
            [musicCardView setMode:@"pause"];
        }
            break;
            
        case 1:
        {
            if ([musicCardView.okAction length]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:musicCardView.okAction]];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)summaryButtonClickedAtIndex:(int)index
{
    switch (index) {
        case 0:
        {
            [UIView animateWithDuration:0.35 animations:^{
                self.visibleCardView.frame = CGRectMake(0, self.visibleCardView.frame.size.height, self.visibleCardView.frame.size.width, self.visibleCardView.frame.size.height);
            } completion:^(BOOL finished) {
                [summaryCardView removeFromSuperview];
                self.visibleCardView = nil;
                self.navigationController.navigationBarHidden = NO;
            }];
        }
            break;
            
        case 1:
        {
            if ([summaryCardView.okAction length]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:summaryCardView.okAction]];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)photoCardButtonClickedAtIndex:(int)index
{
    switch (index)
    {
        case 0:
        {
            [UIView animateWithDuration:0.35 animations:^{
                self.visibleCardView.frame = CGRectMake(0, self.visibleCardView.frame.size.height, self.visibleCardView.frame.size.width, self.visibleCardView.frame.size.height);
            } completion:^(BOOL finished) {
                [photoCardView removeFromSuperview];
                self.visibleCardView = nil;
                self.navigationController.navigationBarHidden = NO;
            }];
        }
            break;
            
        case 1:
        {
            if ([photoCardView.okAction length]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:photoCardView.okAction]];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)pageButtonClickedAtIndex:(int)index
{
    switch (index) {
        case 0:
        {
            [UIView animateWithDuration:0.35 animations:^{
                self.visibleCardView.frame = CGRectMake(0, self.visibleCardView.frame.size.height, self.visibleCardView.frame.size.width, self.visibleCardView.frame.size.height);
            } completion:^(BOOL finished) {
                [pageCardView removeFromSuperview];
                self.visibleCardView = nil;
                self.navigationController.navigationBarHidden = NO;
            }];
        }
            break;
            
        case 1:
        {
            if ([pageCardView.okAction length]) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:pageCardView.okAction]];
            }
        }
            break;
            
        default:
            break;
    }
    
}

- (void)showCard:(MSCard*)card
{
    MSNotification *notif = [self.notificationsDictionary objectForKey:card.notification];
    switch (card.cardType) {
        case MSCardTypeMedia:
        {
            if (!musicCardView) {
                musicCardView = [[[NSBundle mainBundle] loadNibNamed:@"MusicCardView"
                                                               owner:self
                                                             options:nil] firstObject];
            }
            [self.view addSubview:musicCardView];
            musicCardView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            musicCardView.layer.masksToBounds = YES;
            musicCardView.delegate = self;
            musicCardView.titleLabel.text = card.title;
            musicCardView.media = [card.mediaArray firstObject];
            [musicCardView.okButton setTitle:notif.okLabel?:@"OK" forState:UIControlStateNormal];
            musicCardView.okAction = notif.okAction;
            
            self.visibleCardView = musicCardView;
            [UIView animateWithDuration:0.35 animations:^{
                musicCardView.frame = self.view.frame;
                self.navigationController.navigationBarHidden = YES;
            } completion:^(BOOL finished) {
                for (MSMedia *media in visibleCard.mediaArray) {
                    if ([media.contentType containsString:@"video"] && [[media.mediaUrl absoluteString] containsString:@"vimeo"]) {
                        [musicCardView loadVimeoWithUrl:media.mediaUrl];
                    } else if ([media.contentType containsString:@"video"]) {
                        musicCardView.ytPlayerView.delegate = self;
                        [musicCardView loadYoutubeWithUrl:media.mediaUrl];
                    } else if ([media.contentType containsString:@"audio"]) {
                        [musicCardView loadSoundCloudWithUrl:[media.mediaUrl absoluteString]];
                    } else if ([media.contentType containsString:@"video"]) {
                        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Content type is not supported" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    }
                }
            }];
        }
            break;
            
        case MSCardTypePhoto:
        {
            if (!photoCardView) {
                photoCardView = [[[NSBundle mainBundle] loadNibNamed:@"PhotoCardView"
                                                               owner:self
                                                             options:nil] firstObject];
            }
            if (!self.visibleCardView) {
                [self.view addSubview:photoCardView];
                photoCardView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
                photoCardView.layer.masksToBounds = YES;
                photoCardView.imageArray = visibleCard.mediaArray;
                photoCardView.titleLabel.text = visibleCard.title;
                photoCardView.delegate = self;
                [photoCardView.okButton setTitle:notif.okLabel?:@"OK" forState:UIControlStateNormal];
                photoCardView.okAction = notif.okAction;
                self.visibleCardView = photoCardView;
                [photoCardView.imagePager reloadData];
                
                [UIView animateWithDuration:0.35 animations:^{
                    photoCardView.frame = self.view.frame;
                    self.navigationController.navigationBarHidden = YES;
                } completion:^(BOOL finished) {
                }];
            }
        }
            break;
            
        case MSCardTypeSummary:
        {
            if (!summaryCardView) {
                summaryCardView = [[[NSBundle mainBundle] loadNibNamed:@"SummaryCardView"
                                                                 owner:self
                                                               options:nil] firstObject];
            }
            [self.view addSubview:summaryCardView];
            summaryCardView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            summaryCardView.layer.masksToBounds = YES;
            summaryCardView.delegate = self;
            summaryCardView.titleLabel.text = visibleCard.title;
            summaryCardView.summaryView.text = visibleCard.body;
            [summaryCardView.okButton setTitle:notif.okLabel?:@"OK" forState:UIControlStateNormal];
            summaryCardView.summaryView.text = visibleCard.body;
            summaryCardView.okAction = notif.okAction;
            
            MSMedia *media = [visibleCard.mediaArray firstObject];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:media.mediaUrl];
            
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                       if (!error ) {
                                           UIImage *fetchedImage = [UIImage imageWithData:data];
                                           if (fetchedImage) {
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   summaryCardView.imageView.image = fetchedImage;
                                               });
                                           }
                                       }
                                   }];
            
            self.visibleCardView = summaryCardView;
            [UIView animateWithDuration:0.35 animations:^{
                summaryCardView.frame = self.view.frame;
                self.navigationController.navigationBarHidden = YES;
            } completion:^(BOOL finished) {
            }];
        }
            break;
            
        case MSCardTypePage:
        {
            if (!pageCardView) {
                pageCardView = [[[NSBundle mainBundle] loadNibNamed:@"PageCardView"
                                                              owner:self
                                                            options:nil] firstObject];
            }
            if (!self.visibleCardView) {
                [self.view addSubview:pageCardView];
                pageCardView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
                pageCardView.delegate = self;
                pageCardView.titleLabel.text = visibleCard.title;
                pageCardView.summaryView.text = visibleCard.body;
                [pageCardView.okButton setTitle:notif.okLabel?:@"OK" forState:UIControlStateNormal];
                pageCardView.okAction = notif.okAction;
                
                MSMedia *media = [visibleCard.mediaArray firstObject];
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:media.mediaUrl];
                
                [NSURLConnection sendAsynchronousRequest:request
                                                   queue:[NSOperationQueue mainQueue]
                                       completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                           if (!error ) {
                                               UIImage *fetchedImage = [UIImage imageWithData:data];
                                               if (fetchedImage) {
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       pageCardView.imageView.image = fetchedImage;
                                                   });
                                               }
                                           }
                                       }];
                
                self.visibleCardView = pageCardView;
                [UIView animateWithDuration:0.35 animations:^{
                    pageCardView.frame = self.view.frame;
                    self.navigationController.navigationBarHidden = YES;
                } completion:^(BOOL finished) {
                }];
            }
        }
            break;
            
        default:
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"webSegue"]) {
        WebViewController *webVC = (WebViewController*)segue.destinationViewController;
        webVC.url = ((MSWebpageAction*)sender).webUrl;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
