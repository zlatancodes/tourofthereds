//
//  WebViewController.h
//  tourofthereds
//
//  Created on 18/07/17.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property (nonatomic, strong) NSURL *url;

@end
