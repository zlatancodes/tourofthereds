# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

1. Run git clone https://shibbyy@bitbucket.org/zlatancodes/tourofthereds.git
2. cd into the cloned directory
4. Type the command- pod install (this will install the dependencies (Cocoa Pods))
5. Now go to Xcode and sign in using your Certificate.
6. Click on Run (Select your iPhone, you can�t see the functionality of the app in simulator as the app makes intensive of the Bluetooth Low Energy Technology (BLE))
7. Open the Control Centre on your iPhone and enable Bluetooth. Also make sure you have a valid internet connection as we will have texts, images, videos from our backend which needs to be displayed on our app.
8. Follow the instructions on the app and Go to the page with Begin Tour button and click on that.
9. If you are using a Virtual Beacon (another iPhone Device/iPad Device) for testing, we have already configured the app with the default UUID, else just go to the ViewController Class and define the UUID for your beacon.